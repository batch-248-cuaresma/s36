


const express = require("express");



const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

//Route
//The routes are respondible for difining the URIs
//all the business logic is done in the

//route to get all the tasks
//this route expect to reciece

router.get("/",(req,res)=>{

	// taskControllers.getAllTasks();
	//resultFromController
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
})



//Mini Activity Start

// Route to create a new task
// This route expects to receive a POST request at the URL "/tasks"
// The whole URL is at "http://localhost:3001/tasks"
router.post("/", (req, res) => {

	// The "createTask" function needs the data from the request body, so we need to supply it to the function
	// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	//taskController.createTask(req.body);
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
	
})

//Mini-Activity End





router.delete("/:id",(req,res)=>{

	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

})



router.put("/:id",(req,res)=>{

	taskControllers.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


//Activity Session 36
router.get("/:id",(req,res)=>{

	// taskControllers.getAllTasks();
	//resultFromController
	taskControllers.getAllTasks(req.params.id).then(resultFromController => res.send(resultFromController));
})
router.put("/status/:id",(req,res)=>{

	taskControllers.updateStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


//module.exports to export the router object to use in the index.js
module.exports = router;

