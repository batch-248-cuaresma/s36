//Controllers contain the function and business logic
//Meaning all the operations it can do will be

const Task = require("../models/Task");

//Controller function for getting all the task
//Define functions to be used in the taskRouters.js and export 

module.exports.getAllTasks = () => {

return Task.find({}).then(result =>{
	return result;
})


}


//Mini Activity Start
// Controller function for creating a task
// The request body coming from the client was passed from the "taskRoutes.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose model "Task"
	let newTask = new Task({
		
		// Sets the "name" property with the value received from the client/Postman
		name : requestBody.name

	})

	// Saves the newly created "newTask" object in the MongoDB database
	// The "then" method waits until the task is stored in the database or an error is encountered before returning a "true" or "false" value back to the client/Postman
	// The "then" method will accept the following 2 arguments:
		// The first parameter will store the result returned by the Mongoose "save" method
		// The second parameter will store the "error" object
	// Compared to using a callback function on Mongoose methods discussed in the previous session, the first parameter stores the result and the error is stored in the second parameter which is the other way around
		// Ex.
			// newUser.save((saveErr, savedTask) => {})
	return newTask.save().then((task, error) => {

		// If an error is encountered returns a "false" boolean back to the client/Postman
		if (error) {

			console.log(error);
			// If an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// The else statement will no longer be evaluated
			return false;

		// Save successful, returns the new task object back to the client/Postman
		} else {

			return task; 

		}

	})

}

//Mini Activity End


module.exports.deleteTask = (taskId) =>{

	return Task.findByIdAndRemove(taskId).then((removeTask,err)=>{
		if(err){
			console.log(err);
			return false
		}else{
			return `${removeTask.name} has been deleted`;
		}
	})

}


//Controller

module.exports.updateTask = (taskId,newContent) => {

	return Task.findById(taskId).then((result,error)=>{ 
		if(error){
			console.log(error);
			return false
		}
		result.name = newContent.name;
		return result.save().then((updatedTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			}else{
				return updatedTask;
			}
		})
	})

}



//Activity Session 36
module.exports.getAllTasks = (taskId) => {

return Task.findById(taskId).then(result =>{
	return result;
})

}

module.exports.updateStatus = (taskId,newContent) => {

	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error);
			return false
		}
		result.status = newContent.status;
		return result.save().then((updatedStatus,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			}else{
				return updatedStatus;
			}
		})
	})
}

