//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//this allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require("./routes/taskRoutes");

//Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database Connection
//Connecting to MongoDB Atlas

mongoose.set("strictQuery",true)

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.cw5s64h.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))



app.use("/tasks",taskRoutes)



//Server
app.listen(port,()=>console.log(`Now listening to port ${port}`))